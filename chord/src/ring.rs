use crate::{id::Id, message::Message, net, peer::Peer};
use nuuksio::{
    net::{Decoder, Encoder},
    prelude::*,
};
use ros::{fmt, prelude::*};

/// The shared state abstraction of the Chord ring.
#[derive(Debug)]
pub struct Ring {
    // Guarantee: `peers[i - 1].id < peers[i].id < peers[i + 1].id`
    peers: Vec<Peer>,
}

macro_rules! assert_valid {
    ($peers:expr) => {
        if !verify_sorted($peers) {
            panic!("peers are not sorted: {:?}", $peers);
        }
        if !verify_distinct($peers) {
            panic!("peers are not unique: {:?}", $peers);
        }
    };
}

impl Ring {
    pub fn new(id: Id, predecessor: Peer, successors: &[Peer]) -> Self {
        assert!(successors.len() >= 2);
        let mut peers = vec![predecessor, Peer::dummy(id)];
        peers.extend(successors);
        assert_valid!(&peers);
        Self { peers }
    }

    pub fn id(&self) -> Id {
        self.peers[1].id
    }

    pub fn predecessor(&self) -> &Peer {
        &self.peers[0]
    }

    pub fn successor(&self) -> &Peer {
        self.peers.get(2).expect("no successor")
    }

    pub fn successors(&self) -> &[Peer] {
        self.peers.get(2..).expect("no successor")
    }

    pub fn set_predecessor(&mut self, peer: Peer) {
        self.peers[0] = peer;
        assert_valid!(&self.peers);
    }

    pub fn push_successor(&mut self, peer: Peer) {
        trace!("push successor {} in {}", peer, self);
        self.peers.insert(2, peer);
        // Keep the same amount of successors
        self.peers.pop();
        assert_valid!(&self.peers);
    }

    pub fn update_successors(&mut self, peer: &Peer, iter: impl Iterator<Item = Peer>) {
        assert_eq!(peer, self.successor());
        // We keep the first successor
        self.peers.truncate(3);
        // And extend with new successors
        self.peers.extend(iter);
        assert_valid!(&self.peers);
    }

    fn remove_successor(&mut self) {
        self.peers.remove(2);
    }

    // Try a ring query until one successor answer or all fail.
    pub async fn try_query_successor<C>(&mut self, mut codec: C, message: &Message) -> Option<(Peer, Message)>
    where
        C: Encoder<Item = Message>,
        C: Decoder<Item = Message>,
    {
        loop {
            let peer = *self.successor();
            match net::try_query(&mut codec, &peer.addr, message).await {
                Err(e) => {
                    warn!("{} - {}", peer, e);
                    if self.successors().is_empty() {
                        break None;
                    } else {
                        self.remove_successor();
                    }
                }
                Ok(message) => break Some((peer, message)),
            }
        }
    }
}

// aka 'OrderedSuccessorLists' property
fn verify_sorted(peers: &[Peer]) -> bool {
    peers.windows(3).all(|p| p[1].id.between(p[0].id, p[2].id))
}

// aka 'NoDuplicates' property
fn verify_distinct(peers: &[Peer]) -> bool {
    // First peer (predecessor) is ignored because it can be the same as the last successor.
    let peers = &peers[1..];
    peers.iter().enumerate().all(|(i, p)| !peers[i + 1..].contains(p))
}

impl fmt::Display for Ring {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.peers.iter().enumerate().try_for_each(|(index, peer)| {
            match index {
                0 => write!(formatter, "{:#}", peer.id),      // Predecessor
                1 => write!(formatter, " < ({:#})", peer.id), // Self
                _ => write!(formatter, " > {:#}", peer.id),   // Successors
            }
        })
    }
}
