use crate::{net::NetAddr, seed::Seed};
use bincode::{
    de::Decoder,
    enc::Encoder,
    error::{DecodeError, EncodeError},
    impl_borrow_decode, Decode, Encode,
};
use blake3::{Hash, Hasher};
use ros::{cmp::Ordering, fmt};

/// A Chord ring identifier.
#[derive(Copy, Clone, Hash, Eq, PartialEq)]
#[repr(transparent)]
pub struct Id {
    value: Hash,
}

impl Id {
    pub fn between(self, low: Self, high: Self) -> bool {
        if low < high {
            low < self && self < high
        } else {
            low < self || self < high
        }
    }

    // Derive Id from NetAddr and Seed
    pub fn generate(addr: &NetAddr, seed: &Seed) -> Self {
        let mut hasher = Hasher::new_keyed(seed.as_ref());
        addr.hash(&mut hasher);
        Self { value: hasher.finalize() }
    }
}

impl Ord for Id {
    #[inline]
    fn cmp(&self, other: &Self) -> Ordering {
        self.value.as_bytes().cmp(other.value.as_bytes())
    }
}

impl PartialOrd for Id {
    #[inline]
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Encode for Id {
    fn encode<E: Encoder>(&self, encoder: &mut E) -> Result<(), EncodeError> {
        let bytes: &[u8; 32] = self.value.as_bytes();
        bytes.encode(encoder)
    }
}

impl Decode for Id {
    fn decode<D: Decoder>(decoder: &mut D) -> Result<Self, DecodeError> {
        let bytes = <[u8; 32]>::decode(decoder)?;
        Ok(Self { value: Hash::from(bytes) })
    }
}

impl_borrow_decode!(Id);

impl fmt::Debug for Id {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        let high = u16::from_ne_bytes(self.value.as_bytes()[0..2].try_into().unwrap());
        let low = u16::from_ne_bytes(self.value.as_bytes()[30..32].try_into().unwrap());
        write!(formatter, "{:04X}..{:04X}", high, low)
    }
}

impl fmt::Display for Id {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        if formatter.alternate() {
            let value = u16::from_ne_bytes(self.value.as_bytes()[..2].try_into().unwrap());
            write!(formatter, "{:04X}", value)
        } else {
            write!(formatter, "{}", self.value.to_hex())
        }
    }
}
