//! The ring-maintenance protocol.

use crate::{id::Id, net::NetAddr, peer::Peer, version::Version};
use bincode::{Decode, Encode};
use ros::prelude::*;

// TODO `Config` type containing seed, version...

/// The messages defining the ring-maintenance protocol.
#[derive(Debug, Clone, PartialEq, Encode, Decode)]
#[repr(u8)]
pub enum Message {
    Boot {
        version: Version,
        redundancy: u8,
    },
    BootOk {
        id: Id,
    },
    BootErr,

    Status,

    Pending,

    /// Join query message
    /// This message is sent by a joining node to an entry node
    Join {
        version: Version,
        /// The joining node public address.
        addr: NetAddr,
    },
    /// Join forward message
    JoinFw {
        // your predecessor
        predecessor: Peer,
    },
    // JoinOk/JoinErr is sent to the joining node by its predecessor
    /// Join reply message in case of success
    JoinOk {
        // your id
        id: Id,
        // my id
        predecessor_id: Id,
        // your successors
        successors: Box<[Peer]>,
    },
    JoinErr,

    /// Leave query message.
    /// This message is sent by a node when leaving properly.
    Leave {
        /// The identifier of the leaving node.
        id: Id,
    },
    /// Leave reply message.
    LeaveOk,

    Stabilize,
    StabilizeOk {
        id: Id,
        // predecessor of sender
        // receiver check if == self
        predecessor: Peer,
        successors: Box<[Peer]>,
    },

    Rectify {
        peer: Peer,
    },

    ///
    FindPredecessor {
        id: Id,
    },
    PredecessorIsMe,
    PredecessorIs {
        peer: Peer,
    },
    FindPredecessorErr,

    ///
    FindSuccessor {
        id: Id,
    },
    SuccessorIsMe,
    SuccessorIs {
        peer: Peer,
    },
    FindSuccessorErr,

    // to decouple application from chord (and from chord underlying transport protocol and addresses):
    // we could just provide an additional message, as part of the spec, to ask from a new
    // connection to another node, for application purpose. this way it is the only message we ever need
    FindOwner {
        id: Id,
    },
    FindOwnerErr,
    OwnerIs {
        id: Id,
        addr: NetAddr,
    },
}
