//! # RustyChord
//!
//! Chord is a protocol and algorithm for peer-to-peer distributed hash table.
//!
//! (['How to Make Chord Correct'](http://www.pamelazave.com/chord.html)).
//!
//! # Examples
//!
//! Join an existing Chord network with [join](join) and create a new one with [bootstrap](bootstrap).
#![no_std]
#![cfg_attr(not(test), no_main)]
#![warn(missing_debug_implementations)]
#![forbid(unsafe_code)]

mod boot;
mod id;
mod join;
mod message;
mod net;
mod node;
mod peer;
mod ring;
mod scheduler;
mod seed;
mod server;
mod version;
mod worker;

pub use crate::{
    boot::bootstrap,
    id::Id,
    join::join,
    net::NetAddr,
    node::{LockedNode, Node},
    version::{Version, VERSION},
};
