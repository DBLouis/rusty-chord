use crate::{id::Id, message::Message, net::Codec, node::LockedNode, peer::Peer, seed::Seed};
use nuuksio::{
    future,
    net::{Channel, Socket},
    prelude::*,
};
use ros::{
    error::{Error, Result},
    ops::ControlFlow,
    ops::ControlFlow::*,
    prelude::*,
    rc::Rc,
};

#[derive(Debug)]
pub struct Worker {
    // TODO unique global seed
    seed: Seed,
    node: Rc<LockedNode>,
    // This is expensive to create, so we reuse it.
    codec: Option<Box<Codec>>,
}

impl Worker {
    pub fn new(node: Rc<LockedNode>) -> Self {
        let seed = Seed::random();
        let codec = Some(Box::new(Codec::new()));
        Self { seed, node, codec }
    }

    pub async fn run(&mut self, socket: Socket) {
        info!("worker start");

        let codec = self.codec.take().unwrap();
        let mut channel = Channel::new(socket, codec);

        match self.try_run(&mut channel).await {
            Ok(()) => info!("worker done"),
            Err(e) => warn!("worker done: {}", e),
        }

        let (_, mut codec) = channel.into_parts();
        codec.clear();
        assert!(self.codec.replace(codec).is_none());
    }

    async fn try_run(&mut self, channel: &mut Channel<Box<Codec>>) -> Result<()> {
        let mut stop = false;

        while !stop && self.node.is_alive() {
            let message = match self.node.while_alive(channel.recv()).await {
                Ok(Ok(message)) => message,
                Ok(Err(_)) => todo!(),
                Err(Error::Intr) => continue,
                Err(Error::ConnectionReset) => return Ok(()),
                Err(e) => return Err(e),
            };

            let message = match self.reply_to(channel, message).await {
                Continue(message) => message,
                Break(Some(message)) => {
                    stop = true;
                    message
                }
                Break(None) => break,
            };

            loop {
                match channel.send(&message).await {
                    Ok(()) => break,
                    Err(Error::Intr) => continue,
                    Err(e) => return Err(e),
                }
            }

            future::yield_now().await;
        }

        Ok(())
    }

    async fn reply_to(
        &mut self, channel: &mut Channel<Box<Codec>>, message: Message,
    ) -> ControlFlow<Option<Message>, Message> {
        // ring state must not be undefined (when this node updating ring state)
        let node = self.node.get_shared(channel).await;

        match message {
            Message::Stabilize => {
                // TODO can any peer stabilize with me?
                // peer wants to stabilize, send its successor list
                Continue(Message::StabilizeOk {
                    id: node.id(),
                    predecessor: *node.predecessor(),
                    successors: Vec::from(node.successors()).into_boxed_slice(),
                })
            }

            Message::Rectify { peer } => {
                let mut node = self.node.get_exclusive(channel, node).await;
                node.set_predecessor(peer);
                debug!("rectified predecessor is: {}", peer);
                Break(None)
            }

            Message::Join { version, addr } => {
                debug!("peer attempting to join");

                if !version.is_compatible() {
                    debug!("version {} is incompatible", version);
                    return Break(Some(Message::JoinErr));
                }

                // Entry node generates a new ID
                let id = Id::generate(&addr, &self.seed);
                trace!("generated id {}", id);

                let node = &mut *self.node.get_exclusive(channel, node).await;

                if node.is_predecessor_of(id) {
                    // I am the predecessor of joining node
                    debug!("new node {} has successfully joined", id);

                    let message = Message::JoinOk {
                        id,
                        predecessor_id: node.id(),
                        successors: Vec::from(node.successors()).into_boxed_slice(),
                    };

                    let peer = Peer::new(id, addr);
                    node.push_successor(peer);
                    debug!("{:?}", node);

                    Continue(message)
                } else {
                    trace!("finding predecessor of joining node");
                    if let Ok(predecessor) = node.find_predecessor(id).await {
                        debug!("new node predecessor is {}", predecessor);
                        Break(Some(Message::JoinFw { predecessor }))
                    } else {
                        todo!("could not find predecessor of {}", id);
                    }
                }
            }

            Message::FindPredecessor { id } => {
                if node.is_predecessor_of(id) {
                    // I am the predecessor of `id`
                    Continue(Message::PredecessorIsMe)
                } else {
                    let node = &mut *self.node.get_exclusive(channel, node).await;

                    if let Ok(peer) = node.find_predecessor(id).await {
                        Break(Some(Message::PredecessorIs { peer }))
                    } else {
                        trace!("could not find predecessor of {}", id);
                        Continue(Message::FindPredecessorErr)
                    }
                }
            }

            Message::FindSuccessor { id } => {
                if node.is_successor_of(id) {
                    // I am the successor of `id`
                    Continue(Message::SuccessorIsMe)
                } else {
                    let node = &mut *self.node.get_exclusive(channel, node).await;

                    if let Ok(peer) = node.find_successor(id).await {
                        Break(Some(Message::SuccessorIs { peer }))
                    } else {
                        trace!("could not find successor of {}", id);
                        Continue(Message::FindSuccessorErr)
                    }
                }
            }

            Message::FindOwner { id } => {
                if node.is_owner_of(id) {
                    // I am the owner of `id`
                    Continue(Message::OwnerIs { id: node.id(), addr: node.paddr })
                } else {
                    let node = &mut *self.node.get_exclusive(channel, node).await;

                    if let Ok((id, addr)) = node.find_owner(id).await {
                        Break(Some(Message::OwnerIs { id, addr }))
                    } else {
                        trace!("could not find owner of {}", id);
                        Continue(Message::FindOwnerErr)
                    }
                }
            }

            Message::Leave { id } => {
                debug!("node {} is leaving", id);
                Break(Some(Message::LeaveOk))
            }

            Message::Status => {
                trace!("status");
                Break(Some(Message::Status))
            }

            message => {
                warn!("unexpected message: {:?}", message);
                Break(None)
            }
        }
    }
}
