use crate::{
    id::Id,
    message::Message,
    net::{Codec, NetAddr},
    node::{LockedNode, Node},
    peer::Peer,
    ring::Ring,
    scheduler::Scheduler,
    seed::Seed,
    server::Server,
    version::VERSION,
};
use nuuksio::{
    future,
    net::{Channel, Socket},
    prelude::*,
    runtime::Runtime,
    signal::{SignalHandler, SignalSet},
    time::{sleep, timeout},
};
use ros::{
    collections::HashSet,
    convert::TryInto,
    error::{Error, Result},
    libc,
    prelude::*,
    rc::{Rc, Weak},
    time::Duration,
};

const BOOTSTRAP_DELAY: Duration = Duration::from_secs(1);
const BOOTSTRAP_TIMEOUT: Duration = Duration::from_secs(60);

/// Bootstraps a Chord ring.
///
/// Initializes a new Node with the bootstrap procedure.
///
pub async fn bootstrap(caddr: NetAddr, paddr: NetAddr, peers: Vec<NetAddr>) -> Result<Rc<LockedNode>> {
    let rt = Runtime::get();
    let seed = Seed::random();
    trace!("seed={}", seed);

    let socket = caddr.bind().await?;
    debug!("bound to {}", caddr);

    let id = Id::generate(&caddr, &seed);

    debug!("waiting for {}", BOOTSTRAP_DELAY);
    sleep(BOOTSTRAP_DELAY).await?;

    info!("bootstrap node {}", id);

    // Concurrently query each peer and reply to their queries
    let (mut peers, socket) = timeout(
        BOOTSTRAP_TIMEOUT,
        // TODO why use `spawn`?
        future::try_zip(rt.spawn(bootstrap_query(peers.clone())), rt.spawn(bootstrap_reply(socket, id, peers))),
    )
    .await??;

    // Order peers so that my predessor is last
    peers.sort_unstable_by_key(|peer| peer.id);
    let index = peers.binary_search_by_key(&id, |peer| peer.id).unwrap_err();
    peers.rotate_left(index);

    let predecessor = peers.last().copied().unwrap();
    let successors = &peers;
    let ring = Ring::new(id, predecessor, successors);

    debug!("{:?}", ring);

    let node = Node::new(caddr, paddr, ring);
    let node = Rc::new(LockedNode::new(node));

    let signal_handler = {
        let node = Rc::downgrade(&node);
        let mut set = SignalSet::empty();
        set.add(libc::SIGINT);
        set.add(libc::SIGTERM);
        set.add(libc::SIGHUP);
        SignalHandler::new(
            set,
            Box::new(move |_| {
                if let Some(node) = Weak::upgrade(&node) {
                    node.kill();
                }
            }),
        )
    }?;
    rt.spawn(signal_handler.catch());

    let server = Server::new(socket, Rc::clone(&node));
    rt.spawn(server.run());

    let scheduler = Scheduler::new(Rc::clone(&node));
    rt.spawn(scheduler.run());

    info!("bootstrap done");

    Ok(node)
}

async fn bootstrap_reply(mut socket: Socket, id: Id, peers: Vec<NetAddr>) -> Result<Socket> {
    let mut codec = Codec::new();
    let mut received = HashSet::new();

    let redundancy: u8 = peers.len().try_into().unwrap();
    trace!("redundancy {}", redundancy);

    // Wait for one boot message from each peer
    while received.len() < peers.len() {
        let (socket, saddr) = socket.accept().await?;
        let addr: NetAddr = saddr.try_into()?;
        let mut channel = Channel::new(socket, &mut codec);
        debug!("connection from {}", addr);

        if !received.insert(addr) {
            warn!("boot message already received from {}", addr);
            continue;
        }

        let message = channel.recv().await?;

        match message {
            Message::Boot { version, .. } if !version.is_compatible() => {
                error!("version {} is incompatible", version);
                channel.send(&Message::BootErr).await?;
                return Err(Error::ConnectionRefused);
            }
            Message::Boot { redundancy: r, .. } if redundancy != r => {
                error!("redundancy is not the same");
                channel.send(&Message::BootErr).await?;
                return Err(Error::InvalidInput);
            }

            Message::Boot { .. } => {
                debug!("boot message received from {}", addr);
                channel.send(&Message::BootOk { id }).await?;
            }

            message => {
                // bug impl or malicious: abort bootstrap
                warn!("unexpected message");
                debug!("{:?}", message);
                return Err(Error::InvalidInput);
            }
        }
    }

    Ok(socket)
}

async fn bootstrap_query(saddrs: Vec<NetAddr>) -> Result<Vec<Peer>> {
    let redundancy: u8 = saddrs.len().try_into().unwrap();
    trace!("redundancy {}", redundancy);

    let mut peers = Vec::new();

    for saddr in saddrs.iter().copied() {
        let codec = Codec::new();
        let socket = saddr.connect().await?;
        let mut channel = Channel::new(socket, codec);
        debug!("connected to {}", saddr);

        let message = Message::Boot { version: VERSION, redundancy };
        channel.send(&message).await?;

        let message = channel.recv().await?;

        match message {
            Message::BootOk { id } => {
                debug!("peer at {} has ID {:?}", saddr, id);
                peers.push(Peer::new(id, saddr));
            }

            Message::BootErr => {
                error!("bootstrap failed");
                return Err(Error::InvalidInput);
            }

            message => {
                // bug impl or malicious: abort bootstrap
                error!("unexpected message: {:?}", message);
                return Err(Error::InvalidInput);
            }
        }
    }

    Ok(peers)
}
