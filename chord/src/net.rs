use crate::message::Message;
use nuuksio::{
    net::{Channel, Decoder, Encoder},
    prelude::*,
    queue::ByteQueue,
    time::{sleep, timeout},
};
use ros::{
    convert::{TryFrom, TryInto},
    error::{Error, Result},
    prelude::*,
    time::Duration,
};

// NetAddr is opaque so the rest of the code does not rely
// on properties of the actual address type.

#[cfg(feature = "ipv4")]
mod ipv4;
#[cfg(feature = "ipv6")]
mod ipv6;

#[cfg(feature = "ipv4")]
pub use ipv4::*;
#[cfg(feature = "ipv6")]
pub use ipv6::*;

/// How long before considering a peer dead.
pub const DEATH_TIMEOUT: Duration = Duration::from_secs(10);

/// How long to wait before sending a "pending" message.
pub const WAIT_DELAY: Duration = Duration::from_secs(1);

pub async fn try_query<C>(codec: C, addr: &NetAddr, message: &Message) -> Result<Message>
where
    C: Encoder<Item = Message>,
    C: Decoder<Item = Message>,
{
    timeout(DEATH_TIMEOUT, async move {
        let socket = addr.connect().await?;
        let mut channel = Channel::new(socket, codec);
        channel.send(message).await?;
        loop {
            match channel.recv().await {
                Ok(Message::Pending) => {
                    debug!("retrying in {}", WAIT_DELAY);
                    sleep(WAIT_DELAY).await?;
                }
                result => break result,
            }
        }
    })
    .await?
}

// Codec for Chord ring-maintenance messages.
// Uses bincode and 16 bits size prefix.
#[derive(Debug)]
pub struct Codec {
    rx: ByteQueue,
    tx: ByteQueue,
}

impl Codec {
    pub fn new() -> Self {
        let rx = ByteQueue::new().unwrap();
        let tx = ByteQueue::new().unwrap();
        Self { rx, tx }
    }

    pub fn clear(&mut self) {
        self.rx.clear();
        self.tx.clear();
    }
}

impl Encoder for Codec {
    type Item = Message;

    fn push(&mut self, message: &Message) -> Result<()> {
        let config = bincode::config::standard().skip_fixed_array_length();
        let (prefix, buffer) = self.tx.writable().split_at_mut(2);

        let size = bincode::encode_into_slice(message, buffer, config).map_err(|e| {
            warn!("{}", e);
            Error::InvalidInput
        })?;

        if let Ok(value) = u16::try_from(size) {
            prefix.copy_from_slice(&value.to_be_bytes());
        } else {
            return Err(Error::InvalidInput);
        }

        self.tx.push(2 + size);
        Ok(())
    }

    fn buffer(&mut self) -> &mut [u8] {
        self.tx.readable()
    }

    fn advance(&mut self, amount: usize) {
        self.tx.pop(amount)
    }
}

impl Decoder for Codec {
    type Item = Message;

    fn pop(&mut self) -> Result<Option<Message>> {
        let config = bincode::config::standard().skip_fixed_array_length();

        if self.rx.len() < 2 {
            // We need at least two bytes to decode the size.
            return Ok(None);
        }
        let (prefix, buffer) = self.rx.readable().split_at(2);

        let size = u16::from_be_bytes(prefix.try_into().unwrap()) as usize;
        if buffer.len() < size {
            return Ok(None);
        }

        let (message, decoded_size) = bincode::decode_from_slice(&buffer[..size], config).map_err(|e| {
            warn!("{}", e);
            Error::InvalidInput
        })?;
        assert_eq!(size, decoded_size);

        self.rx.pop(2 + size);
        Ok(Some(message))
    }

    fn buffer(&mut self) -> &mut [u8] {
        self.rx.writable()
    }

    fn advance(&mut self, amount: usize) {
        self.rx.push(amount)
    }
}
