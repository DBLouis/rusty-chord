//! ROS - Rust OS library
#![cfg(unix)]
#![no_std]
#![cfg_attr(not(test), no_main)]
#![feature(ptr_metadata)]
#![feature(thread_local)]
#![warn(missing_debug_implementations)]

extern crate alloc as __alloc;
extern crate core as __core;

#[link(name = "c")]
extern "C" {}

#[link(name = "pthread")]
extern "C" {}

pub mod alloc;
pub mod any;
pub mod arch;
pub mod array;
pub mod ascii;
pub mod borrow;
pub mod boxed;
pub mod cell;
pub mod char;
pub mod cmp;
pub mod collections;
pub mod convert;
pub mod error;
pub mod ffi;
pub mod fmt;
pub mod fs;
pub mod future;
pub mod hash;
pub mod hint;
pub mod io;
pub mod iter;
pub mod marker;
pub mod mem;
pub mod num;
pub mod ops;
pub mod panic;
pub mod pin;
pub mod prelude;
pub mod process;
pub mod ptr;
pub mod rc;
pub mod result;
pub mod slice;
pub mod str;
pub mod sync;
pub mod task;
pub mod thread;
pub mod time;
pub mod vec;

#[doc(hidden)]
pub use libc;

#[macro_export]
macro_rules! main {
    ($main:expr) => {
        #[no_mangle]
        pub extern "C" fn main(argc: isize, argv: *const *const i8) -> i32 {
            use $crate::{error::Result, ffi::CStr, libc, vec::Vec};

            let args: Vec<_> = (0..argc)
                .map(|i| unsafe {
                    let ptr = *argv.offset(i);
                    CStr::from_ptr(ptr).to_str().unwrap_or_abort()
                })
                .collect();

            match ($main)(args.as_slice()) {
                Result::Ok(()) => libc::EXIT_SUCCESS,
                Result::Err(e) => {
                    eprintln!("{}", e);
                    libc::EXIT_FAILURE
                }
            }
        }
    };
}
