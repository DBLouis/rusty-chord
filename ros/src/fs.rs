use crate::{
    error::{Error, Result},
    ffi::CStr,
    fmt,
    io::{Fd, Read, Write},
};

#[derive(Debug)]
#[repr(transparent)]
pub struct File {
    fd: Fd,
}

impl File {
    pub fn open(path: &CStr) -> Result<Self> {
        let fd = unsafe { libc::open(path.as_ptr(), libc::O_RDONLY) };
        if fd == -1 {
            Err(Error::last())
        } else {
            Ok(Self { fd })
        }
    }

    pub fn create(path: &CStr) -> Result<Self> {
        let flags = libc::O_WRONLY | libc::O_CREAT | libc::O_TRUNC;
        let mode = libc::S_IRUSR | libc::S_IWUSR | libc::S_IRGRP | libc::S_IROTH;
        let fd = unsafe { libc::open(path.as_ptr(), flags, mode) };
        if fd == -1 {
            Err(Error::last())
        } else {
            Ok(Self { fd })
        }
    }
}

impl Drop for File {
    fn drop(&mut self) {
        let _ = unsafe { libc::close(self.fd) };
    }
}

impl Read for File {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize> {
        let n = unsafe { libc::read(self.fd, buf.as_ptr() as *mut _, buf.len()) };
        if n == -1 {
            Err(Error::last())
        } else {
            Ok(n as usize)
        }
    }
}

impl Write for File {
    fn write(&mut self, buf: &[u8]) -> Result<usize> {
        let n = unsafe { libc::write(self.fd, buf.as_ptr() as *const _, buf.len()) };
        if n == -1 {
            Err(Error::last())
        } else {
            Ok(n as usize)
        }
    }

    fn flush(&mut self) -> Result<()> {
        todo!()
    }
}

impl fmt::Write for File {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        self.write_all(s.as_bytes()).map_err(|_| fmt::Error)
    }
}
