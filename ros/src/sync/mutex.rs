use crate::{
    cell::UnsafeCell,
    error::{Error, Result},
    fmt,
    ops::{Deref, DerefMut},
};

pub struct Mutex<T: ?Sized> {
    pub(super) inner: UnsafeCell<libc::pthread_mutex_t>,
    value: UnsafeCell<T>,
}

unsafe impl<T> Send for Mutex<T> where T: Send {}
unsafe impl<T> Sync for Mutex<T> where T: Send {}

impl<T> Mutex<T> {
    pub const fn new(value: T) -> Self {
        Self { inner: UnsafeCell::new(libc::PTHREAD_MUTEX_INITIALIZER), value: UnsafeCell::new(value) }
    }
}

impl<T: ?Sized> Mutex<T> {
    pub fn lock(&self) -> Result<MutexGuard<'_, T>> {
        let res = unsafe { libc::pthread_mutex_lock(self.inner.get()) };
        if res == 0 {
            Ok(MutexGuard { mutex: self })
        } else {
            Err(Error::from(res))
        }
    }

    pub fn try_lock(&self) -> Result<MutexGuard<'_, T>> {
        let res = unsafe { libc::pthread_mutex_trylock(self.inner.get()) };
        if res == 0 {
            Ok(MutexGuard { mutex: self })
        } else {
            Err(Error::from(res))
        }
    }
}

impl<T: ?Sized> fmt::Debug for Mutex<T> {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        formatter.debug_struct("Mutex").finish_non_exhaustive()
    }
}

pub struct MutexGuard<'a, T: ?Sized> {
    pub(super) mutex: &'a Mutex<T>,
}

impl<'a, T: ?Sized> Deref for MutexGuard<'a, T> {
    type Target = T;

    fn deref(&self) -> &T {
        unsafe { &*self.mutex.value.get() }
    }
}

impl<'a, T: ?Sized> DerefMut for MutexGuard<'a, T> {
    fn deref_mut(&mut self) -> &mut T {
        unsafe { &mut *self.mutex.value.get() }
    }
}

impl<'a, T: ?Sized> Drop for MutexGuard<'a, T> {
    fn drop(&mut self) {
        let res = unsafe { libc::pthread_mutex_unlock(self.mutex.inner.get()) };
        if res != 0 {
            panic!("{}", Error::from(res));
        }
    }
}

impl<'a, T: ?Sized> fmt::Debug for MutexGuard<'a, T> {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        formatter.debug_struct("MutexGuard").finish_non_exhaustive()
    }
}
