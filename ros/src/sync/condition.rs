use super::mutex::MutexGuard;
use crate::{
    cell::UnsafeCell,
    error::{Error, Result},
    fmt,
    time::{Duration, Timestamp},
};

pub struct Condition {
    inner: UnsafeCell<libc::pthread_cond_t>,
}

unsafe impl Send for Condition {}
unsafe impl Sync for Condition {}

impl Condition {
    pub const fn new() -> Self {
        Self { inner: UnsafeCell::new(libc::PTHREAD_COND_INITIALIZER) }
    }

    pub fn wait<'a, T>(&self, guard: MutexGuard<'a, T>) -> Result<MutexGuard<'a, T>> {
        let res = unsafe { libc::pthread_cond_wait(self.inner.get(), guard.mutex.inner.get()) };
        if res == 0 {
            Ok(guard)
        } else {
            Err(Error::from(res))
        }
    }

    pub fn wait_timeout<'a, T>(&self, guard: MutexGuard<'a, T>, duration: Duration) -> Result<MutexGuard<'a, T>> {
        let deadline = Timestamp::now() + duration;
        let res = unsafe { libc::pthread_cond_timedwait(self.inner.get(), guard.mutex.inner.get(), deadline.as_raw()) };
        if res == 0 {
            return Ok(guard);
        }
        match Error::from(res) {
            Error::TimedOut => Ok(guard),
            e => Err(e),
        }
    }

    pub fn notify_one(&self) -> Result<()> {
        let res = unsafe { libc::pthread_cond_signal(self.inner.get()) };
        if res == 0 {
            Ok(())
        } else {
            Err(Error::from(res))
        }
    }

    pub fn notify_all(&self) -> Result<()> {
        let res = unsafe { libc::pthread_cond_broadcast(self.inner.get()) };
        if res == 0 {
            Ok(())
        } else {
            Err(Error::from(res))
        }
    }
}

impl fmt::Debug for Condition {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        formatter.debug_struct("Condition").finish_non_exhaustive()
    }
}
