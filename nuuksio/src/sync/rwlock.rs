use super::semaphore::Semaphore;
use ros::{
    cell::UnsafeCell,
    ops::{Deref, DerefMut},
};

#[derive(Debug)]
pub struct RwLock<T: ?Sized> {
    semaphore: Semaphore,
    value: UnsafeCell<T>,
}

impl<T> RwLock<T> {
    pub fn new(value: T) -> Self {
        Self { semaphore: Semaphore::new(usize::MAX), value: UnsafeCell::new(value) }
    }

    pub fn into_inner(self) -> T {
        self.value.into_inner()
    }
}

impl<T: ?Sized> RwLock<T> {
    pub async fn read(&self) -> RwLockReadGuard<'_, T> {
        self.semaphore.acquire(1).await;
        RwLockReadGuard { rwlock: self }
    }

    pub async fn write(&self) -> RwLockWriteGuard<'_, T> {
        self.semaphore.acquire(usize::MAX).await;
        RwLockWriteGuard { rwlock: self }
    }

    pub fn try_read(&self) -> Option<RwLockReadGuard<'_, T>> {
        if self.semaphore.try_acquire(1) {
            Some(RwLockReadGuard { rwlock: self })
        } else {
            None
        }
    }

    pub fn try_write(&self) -> Option<RwLockWriteGuard<'_, T>> {
        if self.semaphore.try_acquire(usize::MAX) {
            Some(RwLockWriteGuard { rwlock: self })
        } else {
            None
        }
    }
}

#[must_use]
#[derive(Debug)]
pub struct RwLockReadGuard<'a, T: ?Sized> {
    pub(super) rwlock: &'a RwLock<T>,
}

impl<'a, T: ?Sized> Deref for RwLockReadGuard<'a, T> {
    type Target = T;

    fn deref(&self) -> &T {
        unsafe { &*self.rwlock.value.get() }
    }
}

impl<'a, T: ?Sized> Drop for RwLockReadGuard<'a, T> {
    fn drop(&mut self) {
        self.rwlock.semaphore.release(1);
    }
}

#[must_use]
#[derive(Debug)]
pub struct RwLockWriteGuard<'a, T: ?Sized> {
    pub(super) rwlock: &'a RwLock<T>,
}

impl<'a, T: ?Sized> Deref for RwLockWriteGuard<'a, T> {
    type Target = T;

    fn deref(&self) -> &T {
        unsafe { &*self.rwlock.value.get() }
    }
}

impl<'a, T: ?Sized> DerefMut for RwLockWriteGuard<'a, T> {
    fn deref_mut(&mut self) -> &mut T {
        unsafe { &mut *self.rwlock.value.get() }
    }
}

impl<'a, T: ?Sized> Drop for RwLockWriteGuard<'a, T> {
    fn drop(&mut self) {
        self.rwlock.semaphore.release(usize::MAX);
    }
}
