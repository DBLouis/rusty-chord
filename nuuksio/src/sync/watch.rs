use intrusive_collections::{intrusive_adapter, LinkedList, LinkedListLink, UnsafeRef};
use pin_project_lite::pin_project;
use ros::{
    cell::UnsafeCell,
    error::{Error, Result},
    future::Future,
    mem,
    pin::Pin,
    ptr,
    task::{ready, Context, Poll, Waker},
};

intrusive_adapter!(WaiterAdapter = UnsafeRef<Waiter>: Waiter { link: LinkedListLink });

struct Waiter {
    link: LinkedListLink,
    status: UnsafeCell<Status>,
}

enum Status {
    Init,
    Notified,
    Pending(Waker),
}

impl Waiter {
    fn new() -> Self {
        Self { link: LinkedListLink::new(), status: UnsafeCell::new(Status::Init) }
    }
}

#[derive(Debug)]
pub struct Watch {
    queue: UnsafeCell<LinkedList<WaiterAdapter>>,
}

impl Watch {
    pub fn new() -> Self {
        Self { queue: UnsafeCell::new(LinkedList::new(WaiterAdapter::new())) }
    }

    pub async fn with<F>(&self, future: F) -> Result<F::Output>
    where
        F: Future,
    {
        With { watch: self, waiter: Waiter::new(), future }.await
    }

    pub fn notify(&self) {
        let queue = unsafe { &mut *self.queue.get() };

        for waiter in queue.take().into_iter() {
            let status = unsafe { &mut *waiter.status.get() };
            // If waiter is in queue, it has `Pending` status,
            match mem::replace(status, Status::Notified) {
                Status::Pending(waker) => waker.wake(),
                _ => unreachable!(),
            }
        }
    }
}

impl Default for Watch {
    fn default() -> Self {
        Self::new()
    }
}

pin_project! {
    struct With<'a, F> {
        watch: &'a Watch,
        waiter: Waiter,
        #[pin]
        future: F,
    }
}

impl<'a, F> Future for With<'a, F>
where
    F: Future,
{
    type Output = Result<F::Output>;

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let this = self.project();

        let queue = unsafe { &mut *this.watch.queue.get() };
        let status = unsafe { &mut *this.waiter.status.get() };

        match status {
            Status::Init => {
                // First time polled, push in queue.
                let waiter = unsafe { UnsafeRef::from_raw(this.waiter as *const _) };
                queue.push_back(waiter);
                let waker = cx.waker().clone();
                let _ = mem::replace(status, Status::Pending(waker));
            }
            Status::Notified => {
                // Notified, there is not pointer to `self.waiter` in the queue.
                return Poll::Ready(Err(Error::Intr));
            }
            Status::Pending(_) => { /* Already in queue */ }
        }

        // Poll inner future
        let output = ready!(this.future.poll(cx).map(Ok));

        // Inner future is ready, remove `self.waiter` from queue.
        let mut cursor = queue.cursor_mut();
        cursor.move_next();
        while let Some(waiter) = cursor.get() {
            if ptr::eq(waiter, this.waiter) {
                cursor.remove();
            }
            cursor.move_next();
        }

        Poll::Ready(output)
    }
}
