use ros::fmt;

pub const LEVEL: Level = Level::Trace;

#[derive(Debug, Clone, Copy, PartialEq, PartialOrd)]
pub enum Level {
    Trace = 0,
    Debug,
    Info,
    Warn,
    Error,
}

impl fmt::Display for Level {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Level::Trace => write!(formatter, "TRACE"),
            Level::Debug => write!(formatter, "DEBUG"),
            Level::Info => write!(formatter, "INFO_"),
            Level::Warn => write!(formatter, "WARN_"),
            Level::Error => write!(formatter, "ERROR"),
        }
    }
}

#[macro_export]
macro_rules! log {
    ($rt:expr; $level:expr, $fmt:literal $($arg:tt)*) => {
        if $level >= $crate::log::LEVEL {
            let ts = ros::time::Timestamp::now();
            let tid = $rt.get_tid();
            let path = module_path!();
            $rt.log(format_args!("[{}][{}][{:.>24}:{:03}][{}] {}\n",
                ts, $level, &path[..path.len().min(24)], line!(), tid,
                format_args!($fmt $($arg)*)
            ));
        }
    };
    ($level:expr, $fmt:literal $($arg:tt)*) => {
        {
            let rt = $crate::runtime::Runtime::get();
            $crate::log!(rt; $level, $fmt $($arg)*);
        }
    };
}

#[macro_export]
macro_rules! trace {
    ($rt:expr; $fmt:literal $($arg:tt)*) => { $crate::log!($rt; $crate::log::Level::Trace, $fmt $($arg)*) };
    ($fmt:literal $($arg:tt)*) => { $crate::log!($crate::log::Level::Trace, $fmt $($arg)*) };
}

#[macro_export]
macro_rules! debug {
    ($rt:expr; $fmt:literal $($arg:tt)*) => { $crate::log!($rt; $crate::log::Level::Debug, $fmt $($arg)*) };
    ($fmt:literal $($arg:tt)*) => { $crate::log!($crate::log::Level::Debug, $fmt $($arg)*) };
}

#[macro_export]
macro_rules! info {
    ($rt:expr; $fmt:literal $($arg:tt)*) => { $crate::log!($rt; $crate::log::Level::Info, $fmt $($arg)*) };
    ($fmt:literal $($arg:tt)*) => { $crate::log!($crate::log::Level::Info, $fmt $($arg)*) };
}

#[macro_export]
macro_rules! warn {
    ($rt:expr; $fmt:literal $($arg:tt)*) => { $crate::log!($rt; $crate::log::Level::Warn, $fmt $($arg)*) };
    ($fmt:literal $($arg:tt)*) => { $crate::log!($crate::log::Level::Warn, $fmt $($arg)*) };
}

#[macro_export]
macro_rules! error {
    ($rt:expr; $fmt:literal $($arg:tt)*) => { $crate::log!($rt; $crate::log::Level::Error, $fmt $($arg)*) };
    ($fmt:literal $($arg:tt)*) => { $crate::log!($crate::log::Level::Error, $fmt $($arg)*) };
}

#[macro_export]
macro_rules! mod_level {
    ($level:expr) => {
        const __MOD_LEVEL: $crate::log::Level = $level;
    };
}

#[macro_export]
macro_rules! mod_trace {
    ($rt:expr; $fmt:literal $($arg:tt)*) => {
        if __MOD_LEVEL <= $crate::log::Level::Trace { $crate::trace!($rt; $fmt $($arg)*) }
    };
    ($fmt:literal $($arg:tt)*) => {
        {
            let rt = $crate::runtime::Runtime::get();
            $crate::mod_trace!(rt; $fmt $($arg:tt)*);
        }
    };
}

#[macro_export]
macro_rules! mod_debug {
    ($rt:expr; $fmt:literal $($arg:tt)*) => {
        if __MOD_LEVEL <= $crate::log::Level::Debug { $crate::debug!($rt; $fmt $($arg)*) }
    };
    ($fmt:literal $($arg:tt)*) => {
        {
            let rt = $crate::runtime::Runtime::get();
            $crate::mod_debug!(rt; $fmt $($arg:tt)*);
        }
    };
}
