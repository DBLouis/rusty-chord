use super::address::{AnySocketAddr, Family, SocketAddr};
use crate::{reactor::Interest, runtime::Runtime};
use ros::{
    error::{Error, Result},
    future::poll_fn,
    io::Fd,
    mem,
    mem::MaybeUninit,
    task::{Context, Poll},
    time::Duration,
};

#[derive(Debug, Clone, Copy, Eq, Hash, PartialEq)]
#[non_exhaustive]
#[repr(i32)]
pub enum SocketType {
    Stream = libc::SOCK_STREAM,
    Datagram = libc::SOCK_DGRAM,
}

#[derive(Debug, Clone, Copy, Eq, Hash, PartialEq)]
#[non_exhaustive]
#[repr(i32)]
pub enum Protocol {
    Auto = libc::IPPROTO_IP,
    Udp = libc::IPPROTO_UDP,
    Tcp = libc::IPPROTO_TCP,
    Sctp = libc::IPPROTO_SCTP,
    Raw = libc::IPPROTO_RAW,
}

#[derive(Debug, Clone, Copy, Eq, Hash, PartialEq)]
#[repr(i32)]
pub enum Shutdown {
    Read = libc::SHUT_RD,
    Write = libc::SHUT_WR,
    Both = libc::SHUT_RDWR,
}

#[derive(Debug)]
#[repr(transparent)]
pub struct Socket {
    rt: Runtime,
    fd: Fd,
}

impl Socket {
    pub async fn bind(family: Family, stype: SocketType, protocol: Protocol, saddr: &SocketAddr) -> Result<Self> {
        let rt = Runtime::get();
        debug_assert_eq!(saddr.family(), family);
        let flags = libc::SOCK_CLOEXEC | libc::SOCK_NONBLOCK;
        let fd = socket(family as _, stype as _, flags, protocol as _)?;
        reuse_addr(fd)?;
        poll_fn(move |cx| poll_bind(rt, cx, fd, saddr)).await?;
        listen(fd)?;
        Ok(Self { rt, fd })
    }

    pub async fn accept(&mut self) -> Result<(Self, AnySocketAddr)> {
        let rt = self.rt;
        let (fd, saddr) = poll_fn(move |cx| self.poll_accept(cx)).await?;
        Ok((Self { rt, fd }, saddr))
    }

    pub async fn connect(family: Family, stype: SocketType, protocol: Protocol, saddr: &SocketAddr) -> Result<Self> {
        let rt = Runtime::get();
        debug_assert_eq!(saddr.family(), family);
        let flags = libc::SOCK_CLOEXEC | libc::SOCK_NONBLOCK;
        let fd = socket(family as _, stype as _, flags, protocol as _)?;
        poll_fn(move |cx| poll_connect(rt, cx, fd, saddr)).await?;
        Ok(Self { rt, fd })
    }

    pub fn poll_accept(&mut self, cx: &mut Context<'_>) -> Poll<Result<(Fd, AnySocketAddr)>> {
        match accept(self.fd) {
            Ok(x) => Poll::Ready(Ok(x)),
            Err(Error::WouldBlock) => {
                self.rt.register(self.fd, Interest::readable(), cx.waker().clone());
                Poll::Pending
            }
            Err(e) => Poll::Ready(Err(e)),
        }
    }

    pub async fn recv(&mut self, buf: &mut [u8]) -> Result<usize> {
        poll_fn(move |cx| self.poll_recv(cx, buf)).await
    }

    pub async fn send(&mut self, buf: &[u8]) -> Result<usize> {
        poll_fn(move |cx| self.poll_send(cx, buf)).await
    }

    pub fn set_recv_timeout(&self, duration: Option<Duration>) -> Result<()> {
        set_timeout(self.fd, libc::SO_RCVTIMEO, duration)
    }

    pub fn set_send_timeout(&self, duration: Option<Duration>) -> Result<()> {
        set_timeout(self.fd, libc::SO_SNDTIMEO, duration)
    }

    fn poll_recv(&mut self, cx: &mut Context<'_>, buf: &mut [u8]) -> Poll<Result<usize>> {
        match recv(self.fd, buf, 0) {
            Ok(0) => Poll::Ready(Err(Error::ConnectionReset)),
            Ok(n) => Poll::Ready(Ok(n)),
            Err(Error::WouldBlock) => {
                self.rt.register(self.fd, Interest::readable(), cx.waker().clone());
                Poll::Pending
            }
            Err(e) => Poll::Ready(Err(e)),
        }
    }

    fn poll_send(&mut self, cx: &mut Context<'_>, buf: &[u8]) -> Poll<Result<usize>> {
        match send(self.fd, buf, 0) {
            Ok(0) => Poll::Ready(Err(Error::ConnectionReset)),
            Ok(n) => Poll::Ready(Ok(n)),
            Err(Error::WouldBlock) => {
                self.rt.register(self.fd, Interest::writable(), cx.waker().clone());
                Poll::Pending
            }
            Err(e) => Poll::Ready(Err(e)),
        }
    }

    pub fn shutdown(&mut self, op: Shutdown) -> Result<()> {
        shutdown(self.fd, op as _)
    }
}

impl Drop for Socket {
    fn drop(&mut self) {
        self.rt.unregister(self.fd);
        let _ = unsafe { libc::close(self.fd) };
    }
}

fn socket(family: libc::c_int, stype: libc::c_int, flags: libc::c_int, protocol: libc::c_int) -> Result<Fd> {
    let fd = unsafe { libc::socket(family, stype | flags, protocol) };
    if fd == -1 {
        Err(Error::last())
    } else {
        Ok(fd)
    }
}

fn bind(fd: Fd, saddr: &SocketAddr) -> Result<()> {
    let rv = unsafe { libc::bind(fd, saddr.as_ref(), saddr.len()) };
    if rv == -1 {
        Err(Error::last())
    } else {
        Ok(())
    }
}

fn reuse_addr(fd: Fd) -> Result<()> {
    let rv = unsafe {
        let arg: i32 = 1;
        let arg = &arg as *const _ as *const libc::c_void;
        libc::setsockopt(fd, libc::SOL_SOCKET, libc::SO_REUSEADDR, arg, 4)
    };
    if rv == -1 {
        Err(Error::last())
    } else {
        Ok(())
    }
}

fn listen(fd: Fd) -> Result<()> {
    let rv = unsafe { libc::listen(fd, 0) };
    if rv == -1 {
        Err(Error::last())
    } else {
        Ok(())
    }
}

fn accept(fd: Fd) -> Result<(Fd, AnySocketAddr)> {
    let mut saddr = MaybeUninit::uninit();
    let mut len = mem::size_of_val(&saddr) as libc::socklen_t;

    let flags = libc::SOCK_CLOEXEC | libc::SOCK_NONBLOCK;
    let fd = unsafe {
        let saddr = saddr.as_mut_ptr();
        libc::accept4(fd, saddr as *mut _, &mut len, flags)
    };

    if fd == -1 {
        Err(Error::last())
    } else {
        let saddr = unsafe { saddr.assume_init() };
        Ok((fd, saddr))
    }
}

fn connect(fd: Fd, saddr: &SocketAddr) -> Result<()> {
    let rv = unsafe { libc::connect(fd, saddr.as_ref(), saddr.len()) };
    if rv == -1 {
        Err(Error::last())
    } else {
        Ok(())
    }
}

fn recv(fd: Fd, buf: &mut [u8], flags: i32) -> Result<usize> {
    let rv = unsafe {
        let ptr = buf.as_mut_ptr() as *mut _;
        libc::recv(fd, ptr, buf.len(), flags)
    };
    if rv == -1 {
        Err(Error::last())
    } else {
        Ok(rv as usize)
    }
}

fn send(fd: Fd, buf: &[u8], flags: i32) -> Result<usize> {
    let rv = unsafe {
        let ptr = buf.as_ptr() as *const _;
        libc::send(fd, ptr, buf.len(), flags)
    };
    if rv == -1 {
        Err(Error::last())
    } else {
        Ok(rv as usize)
    }
}

fn set_timeout(fd: Fd, arg: libc::c_int, duration: Option<Duration>) -> Result<()> {
    let duration = if let Some(duration) = duration {
        libc::timeval { tv_sec: duration.as_secs() as _, tv_usec: duration.subsec_micros() as _ }
    } else {
        libc::timeval { tv_sec: 0, tv_usec: 0 }
    };
    let rv = unsafe {
        let ptr = &duration as *const _ as *const libc::c_void;
        let len = mem::size_of::<libc::timeval>() as u32;
        libc::setsockopt(fd, libc::SOL_SOCKET, arg, ptr, len)
    };
    if rv == -1 {
        Err(Error::last())
    } else {
        Ok(())
    }
}

fn shutdown(fd: Fd, op: libc::c_int) -> Result<()> {
    let rv = unsafe { libc::shutdown(fd, op) };
    if rv == -1 {
        Err(Error::last())
    } else {
        Ok(())
    }
}

fn poll_bind(rt: Runtime, cx: &mut Context<'_>, fd: Fd, saddr: &SocketAddr) -> Poll<Result<()>> {
    match bind(fd, saddr) {
        Ok(_) => Poll::Ready(Ok(())),
        Err(Error::InProgress) => {
            rt.register(fd, Interest::writable(), cx.waker().clone());
            Poll::Pending
        }
        Err(e) => Poll::Ready(Err(e)),
    }
}

fn poll_connect(rt: Runtime, cx: &mut Context<'_>, fd: Fd, saddr: &SocketAddr) -> Poll<Result<()>> {
    match connect(fd, saddr) {
        Ok(_) => Poll::Ready(Ok(())),
        Err(Error::InProgress) => {
            rt.register(fd, Interest::writable(), cx.waker().clone());
            Poll::Pending
        }
        Err(e) => Poll::Ready(Err(e)),
    }
}
