use crate::{log::Level, net::Socket, prelude::*, runtime::Runtime};
use pretty_hex::*;
use ros::{error::Result, fmt, prelude::*};

// TODO depending on socket type, a length prefix might be prepended.
// datagram are always received one by one, but that's different for streams

// TODO datagram are always recv'd entirely or not at all. but if the buffer
// is too small, the remaining bytes are lost. so we must ensure the buffer is large enough
// https://stackoverflow.com/questions/1098897/what-is-the-largest-safe-udp-packet-size-on-the-internet

// TODO careful to not ignore bytes from following not yet decodec item(s)
// this can happen with stream sockets, e.g. TCP

mod_level!(Level::Debug);

pub trait Encoder {
    type Item;
    fn push(&mut self, item: &Self::Item) -> Result<()>;
    fn buffer(&mut self) -> &mut [u8];
    fn advance(&mut self, amount: usize);
}

pub trait Decoder {
    type Item;
    fn pop(&mut self) -> Result<Option<Self::Item>>;
    fn buffer(&mut self) -> &mut [u8];
    fn advance(&mut self, amount: usize);
}

#[derive(Debug)]
pub struct Channel<C> {
    rt: Runtime,
    socket: Socket,
    codec: C,
}

impl<C> Channel<C> {
    pub fn new(socket: Socket, codec: C) -> Self {
        let rt = Runtime::get();
        Self { rt, socket, codec }
    }

    pub fn into_parts(self) -> (Socket, C) {
        (self.socket, self.codec)
    }
}

impl<C> Channel<C>
where
    C: Encoder,
    C::Item: fmt::Debug,
{
    pub async fn send(&mut self, item: &C::Item) -> Result<()> {
        mod_debug!(self.rt; "send {:?}", &item);
        self.codec.push(item)?;
        loop {
            let buffer = self.codec.buffer();
            if buffer.is_empty() {
                break Ok(());
            }

            mod_trace!(self.rt; "{:#?}", buffer.hex_dump());

            let n = self.socket.send(buffer).await?;
            debug_assert_ne!(n, 0);
            self.codec.advance(n);
        }
    }
}

impl<C> Channel<C>
where
    C: Decoder,
    C::Item: fmt::Debug,
{
    pub async fn recv(&mut self) -> Result<C::Item> {
        loop {
            match self.codec.pop()? {
                None => {
                    let buffer = self.codec.buffer();
                    let n = self.socket.recv(buffer).await?;
                    debug_assert_ne!(n, 0);

                    mod_trace!(self.rt; "{:#?}", buffer[..n].hex_dump());

                    self.codec.advance(n);
                }
                Some(item) => {
                    mod_debug!(self.rt; "recv {:?}", &item);
                    break Ok(item);
                }
            }
        }
    }
}

/* Forward impls */

impl<C: Encoder> Encoder for &mut C {
    type Item = C::Item;
    #[inline]
    fn push(&mut self, item: &Self::Item) -> Result<()> {
        C::push(*self, item)
    }
    #[inline]
    fn buffer(&mut self) -> &mut [u8] {
        C::buffer(*self)
    }
    #[inline]
    fn advance(&mut self, amount: usize) {
        C::advance(*self, amount)
    }
}

impl<C: Encoder> Encoder for Box<C> {
    type Item = C::Item;
    #[inline]
    fn push(&mut self, item: &Self::Item) -> Result<()> {
        C::push(&mut *self, item)
    }
    #[inline]
    fn buffer(&mut self) -> &mut [u8] {
        C::buffer(&mut *self)
    }
    #[inline]
    fn advance(&mut self, amount: usize) {
        C::advance(&mut *self, amount)
    }
}

impl<C: Decoder> Decoder for &mut C {
    type Item = C::Item;
    #[inline]
    fn pop(&mut self) -> Result<Option<Self::Item>> {
        C::pop(*self)
    }
    #[inline]
    fn buffer(&mut self) -> &mut [u8] {
        C::buffer(*self)
    }
    #[inline]
    fn advance(&mut self, amount: usize) {
        C::advance(*self, amount)
    }
}

impl<C: Decoder> Decoder for Box<C> {
    type Item = C::Item;
    #[inline]
    fn pop(&mut self) -> Result<Option<Self::Item>> {
        C::pop(&mut *self)
    }
    #[inline]
    fn buffer(&mut self) -> &mut [u8] {
        C::buffer(&mut *self)
    }
    #[inline]
    fn advance(&mut self, amount: usize) {
        C::advance(&mut *self, amount)
    }
}
