use ros::{
    convert::TryFrom,
    error::Error,
    ffi::CStr,
    fmt,
    hash::{Hash, Hasher},
    mem, slice,
    str::FromStr,
};

#[derive(Debug, Clone, Copy, Eq, Hash, PartialEq)]
#[non_exhaustive]
#[repr(i32)]
pub enum Family {
    Unix = libc::AF_UNIX,
    Ipv4 = libc::AF_INET,
    Ipv6 = libc::AF_INET6,
}

#[derive(Clone, Copy)]
#[repr(transparent)]
pub struct AnySocketAddr {
    #[allow(dead_code)]
    inner: libc::sockaddr_storage,
}

impl AnySocketAddr {
    pub fn family(&self) -> Family {
        self.as_ref().family()
    }
}

impl AsRef<SocketAddr> for AnySocketAddr {
    fn as_ref(&self) -> &SocketAddr {
        unsafe { &*(self as *const Self as *const SocketAddr) }
    }
}

impl AsMut<SocketAddr> for AnySocketAddr {
    fn as_mut(&mut self) -> &mut SocketAddr {
        unsafe { &mut *(self as *mut Self as *mut SocketAddr) }
    }
}

impl fmt::Debug for AnySocketAddr {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        formatter.debug_struct("AnySocketAddr").field("family", &self.inner.ss_family).finish_non_exhaustive()
    }
}

impl fmt::Display for AnySocketAddr {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Display::fmt(self.as_ref(), formatter)
    }
}

impl From<Ipv4SocketAddr> for AnySocketAddr {
    fn from(o: Ipv4SocketAddr) -> Self {
        Self { inner: unsafe { *(&o.inner as *const libc::sockaddr_in as *const _) } }
    }
}

impl From<Ipv6SocketAddr> for AnySocketAddr {
    fn from(o: Ipv6SocketAddr) -> Self {
        Self { inner: unsafe { *(&o.inner as *const libc::sockaddr_in6 as *const _) } }
    }
}

impl From<UnixSocketAddr> for AnySocketAddr {
    fn from(o: UnixSocketAddr) -> Self {
        Self { inner: unsafe { *(&o.inner as *const libc::sockaddr_un as *const _) } }
    }
}

impl TryFrom<AnySocketAddr> for Ipv4SocketAddr {
    type Error = Error;

    fn try_from(o: AnySocketAddr) -> Result<Self, Error> {
        if o.family() == Family::Ipv4 {
            Ok(Self { inner: unsafe { *(&o.inner as *const libc::sockaddr_storage as *const _) } })
        } else {
            Err(Error::InvalidInput)
        }
    }
}

impl TryFrom<AnySocketAddr> for Ipv6SocketAddr {
    type Error = Error;

    fn try_from(o: AnySocketAddr) -> Result<Self, Error> {
        if o.family() == Family::Ipv6 {
            Ok(Self { inner: unsafe { *(&o.inner as *const libc::sockaddr_storage as *const _) } })
        } else {
            Err(Error::InvalidInput)
        }
    }
}

impl TryFrom<AnySocketAddr> for UnixSocketAddr {
    type Error = Error;

    fn try_from(o: AnySocketAddr) -> Result<Self, Error> {
        if o.family() == Family::Unix {
            Ok(Self { inner: unsafe { *(&o.inner as *const libc::sockaddr_storage as *const _) } })
        } else {
            Err(Error::InvalidInput)
        }
    }
}

#[repr(transparent)]
pub struct SocketAddr {
    inner: libc::sockaddr,
}

impl SocketAddr {
    pub fn family(&self) -> Family {
        match self.inner.sa_family as libc::c_int {
            libc::AF_INET => Family::Ipv4,
            libc::AF_INET6 => Family::Ipv6,
            family => todo!("unknown family {:#X}", family),
        }
    }

    pub(crate) fn len(&self) -> libc::socklen_t {
        (match self.inner.sa_family as libc::c_int {
            libc::AF_INET => mem::size_of::<libc::sockaddr_in>(),
            libc::AF_INET6 => mem::size_of::<libc::sockaddr_in6>(),
            family => todo!("unknown family {:#X}", family),
        }) as _
    }
}

impl AsRef<libc::sockaddr> for SocketAddr {
    fn as_ref(&self) -> &libc::sockaddr {
        &self.inner
    }
}

impl fmt::Debug for SocketAddr {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        formatter.debug_struct("SocketAddr").field("family", &self.inner.sa_family).finish_non_exhaustive()
    }
}

impl fmt::Display for SocketAddr {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        let (ptr, _) = (self as *const Self).to_raw_parts();
        match self.inner.sa_family as libc::c_int {
            libc::AF_INET => {
                let saddr: &Ipv4SocketAddr = unsafe { &*(ptr as *const Ipv4SocketAddr) };
                saddr.fmt(formatter)
            }
            libc::AF_INET6 => {
                let saddr: &Ipv6SocketAddr = unsafe { &*(ptr as *const Ipv6SocketAddr) };
                saddr.fmt(formatter)
            }
            _ => todo!(),
        }
    }
}

#[repr(transparent)]
pub struct UnixSocketAddr {
    inner: libc::sockaddr_un,
}

impl UnixSocketAddr {
    pub fn new(path: &CStr) -> Self {
        let mut inner: libc::sockaddr_un = unsafe { mem::zeroed() };
        inner.sun_family = libc::AF_UNIX as u16;
        let path = path.to_bytes();
        // Maximum length -1 for NUL byte
        let len = path.len().min(inner.sun_path.len() - 1);
        let bytes = unsafe { slice::from_raw_parts(path.as_ptr() as _, len) };
        // No need to copy NUL byte because struct is zeroed
        inner.sun_path[..len].copy_from_slice(bytes);
        Self { inner }
    }
}

impl fmt::Debug for UnixSocketAddr {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(formatter, "{:?}", self.inner.sun_path.as_ref())
    }
}

impl fmt::Display for UnixSocketAddr {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        // `sun_path` ends with at least one NUL byte
        let path = self.inner.sun_path.split_inclusive(|x| *x == 0).next().unwrap();
        let bytes = unsafe { slice::from_raw_parts(path.as_ptr() as *const u8, path.len()) };
        let path = CStr::from_bytes_with_nul(bytes).unwrap();
        write!(formatter, "{}", path.to_string_lossy())
    }
}

#[derive(Clone, Copy)]
#[repr(transparent)]
pub struct Ipv4Addr {
    pub(crate) inner: libc::in_addr,
}

impl Ipv4Addr {
    pub const UNSPECIFIED: Self = Self::new(0, 0, 0, 0);
    pub const LOCALHOST: Self = Self::new(127, 0, 0, 1);

    pub const fn new(a: u8, b: u8, c: u8, d: u8) -> Self {
        Self { inner: libc::in_addr { s_addr: u32::from_ne_bytes([a, b, c, d]) } }
    }

    pub const fn octets(&self) -> [u8; 4] {
        self.inner.s_addr.to_ne_bytes()
    }
}

impl PartialEq for Ipv4Addr {
    fn eq(&self, other: &Self) -> bool {
        self.inner.s_addr == other.inner.s_addr
    }
}

impl Eq for Ipv4Addr {}

impl Hash for Ipv4Addr {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.inner.s_addr.hash(state)
    }
}

impl From<u32> for Ipv4Addr {
    fn from(value: u32) -> Self {
        Self { inner: libc::in_addr { s_addr: value } }
    }
}

impl From<Ipv4Addr> for u32 {
    fn from(addr: Ipv4Addr) -> Self {
        addr.inner.s_addr
    }
}

impl fmt::Debug for Ipv4Addr {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Display::fmt(self, formatter)
    }
}

impl fmt::Display for Ipv4Addr {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        let [a, b, c, d] = self.octets();
        write!(formatter, "{}.{}.{}.{}", a, b, c, d)
    }
}

impl FromStr for Ipv4Addr {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut bytes = [0; 4];
        let mut parts = s.split('.');

        for b in bytes.iter_mut() {
            let v = parts.next().ok_or(Error::InvalidInput)?;
            *b = v.parse().map_err(|_| Error::InvalidInput)?;
        }

        Ok(Self { inner: libc::in_addr { s_addr: u32::from_ne_bytes(bytes) } })
    }
}

#[derive(Clone, Copy)]
#[repr(transparent)]
pub struct Ipv6Addr {
    pub(crate) inner: libc::in6_addr,
}

impl Ipv6Addr {
    pub const UNSPECIFIED: Self = Self::new(0, 0, 0, 0, 0, 0, 0, 0);
    pub const LOCALHOST: Self = Self::new(0, 0, 0, 0, 0, 0, 0, 1);

    #[allow(clippy::too_many_arguments)]
    pub const fn new(a: u16, b: u16, c: u16, d: u16, e: u16, f: u16, g: u16, h: u16) -> Self {
        let array = [a.to_be(), b.to_be(), c.to_be(), d.to_be(), e.to_be(), f.to_be(), g.to_be(), h.to_be()];
        Self { inner: libc::in6_addr { s6_addr: unsafe { mem::transmute(array) } } }
    }

    pub const fn octets(&self) -> [u8; 16] {
        self.inner.s6_addr
    }

    pub const fn segments(&self) -> [u16; 8] {
        unsafe { mem::transmute(self.inner.s6_addr) }
    }
}

impl PartialEq for Ipv6Addr {
    fn eq(&self, other: &Self) -> bool {
        self.inner.s6_addr == other.inner.s6_addr
    }
}

impl Eq for Ipv6Addr {}

impl Hash for Ipv6Addr {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.inner.s6_addr.hash(state)
    }
}

impl fmt::Debug for Ipv6Addr {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Display::fmt(self, formatter)
    }
}

impl fmt::Display for Ipv6Addr {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        let [a, b, c, d, e, f, g, h] = self.segments();
        write!(formatter, "{:X}:{:X}:{:X}:{:X}:{:X}:{:X}:{:X}:{:X}", a, b, c, d, e, f, g, h)
    }
}

#[derive(Clone, Copy)]
#[repr(transparent)]
pub struct Ipv4SocketAddr {
    inner: libc::sockaddr_in,
}

impl Ipv4SocketAddr {
    pub const fn new(ip: Ipv4Addr, port: u16) -> Self {
        Self {
            inner: libc::sockaddr_in {
                sin_family: libc::AF_INET as u16,
                sin_addr: ip.inner,
                sin_port: port,
                sin_zero: [0; 8],
            },
        }
    }

    pub const fn ip(&self) -> &Ipv4Addr {
        unsafe { mem::transmute(&self.inner.sin_addr) }
    }

    pub const fn port(&self) -> u16 {
        self.inner.sin_port
    }
}

impl AsRef<SocketAddr> for Ipv4SocketAddr {
    fn as_ref(&self) -> &SocketAddr {
        unsafe { &*(self as *const Self as *const SocketAddr) }
    }
}

impl PartialEq for Ipv4SocketAddr {
    fn eq(&self, other: &Self) -> bool {
        self.inner.sin_family == other.inner.sin_family
            && self.inner.sin_port == other.inner.sin_port
            && self.inner.sin_addr.s_addr == other.inner.sin_addr.s_addr
    }
}

impl Eq for Ipv4SocketAddr {}

impl Hash for Ipv4SocketAddr {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.inner.sin_family.hash(state);
        self.inner.sin_port.hash(state);
        self.inner.sin_addr.s_addr.hash(state);
    }
}

impl fmt::Debug for Ipv4SocketAddr {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Display::fmt(self, formatter)
    }
}

impl fmt::Display for Ipv4SocketAddr {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        let addr = Ipv4Addr { inner: self.inner.sin_addr };
        write!(formatter, "{}:{}", addr, self.inner.sin_port)
    }
}

impl FromStr for Ipv4SocketAddr {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (ip, port) = s.split_once(':').ok_or(Error::InvalidInput)?;
        let ip = ip.parse()?;
        let port = port.parse().map_err(|_| Error::InvalidInput)?;
        Ok(Self::new(ip, port))
    }
}

#[derive(Clone, Copy)]
#[repr(transparent)]
pub struct Ipv6SocketAddr {
    inner: libc::sockaddr_in6,
}

impl Ipv6SocketAddr {
    pub fn new(ip: Ipv6Addr, port: u16) -> Self {
        Self {
            inner: libc::sockaddr_in6 {
                sin6_family: libc::AF_INET6 as u16,
                sin6_port: port,
                sin6_flowinfo: 0,
                sin6_addr: ip.inner,
                sin6_scope_id: 0,
            },
        }
    }

    pub const fn ip(&self) -> &Ipv6Addr {
        unsafe { mem::transmute(&self.inner.sin6_addr) }
    }

    pub const fn port(&self) -> u16 {
        self.inner.sin6_port
    }
}

impl AsRef<SocketAddr> for Ipv6SocketAddr {
    fn as_ref(&self) -> &SocketAddr {
        unsafe { &*(self as *const Self as *const SocketAddr) }
    }
}

impl PartialEq for Ipv6SocketAddr {
    fn eq(&self, other: &Self) -> bool {
        self.inner.sin6_family == other.inner.sin6_family
            && self.inner.sin6_port == other.inner.sin6_port
            && self.inner.sin6_addr.s6_addr == other.inner.sin6_addr.s6_addr
    }
}

impl Eq for Ipv6SocketAddr {}

impl Hash for Ipv6SocketAddr {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.inner.sin6_family.hash(state);
        self.inner.sin6_port.hash(state);
        self.inner.sin6_addr.s6_addr.hash(state);
    }
}

impl fmt::Debug for Ipv6SocketAddr {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Display::fmt(self, formatter)
    }
}

impl fmt::Display for Ipv6SocketAddr {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        let addr = Ipv6Addr { inner: self.inner.sin6_addr };
        write!(formatter, "{}:{}", addr, self.inner.sin6_port)
    }
}
