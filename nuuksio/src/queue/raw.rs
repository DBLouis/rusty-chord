use super::file::File;
use ros::{alloc::Layout, error::Result, ptr::NonNull, slice};

#[derive(Debug)]
pub struct RawQueue {
    file: File,
    head: usize,
    tail: usize,
}

impl RawQueue {
    pub fn new() -> Result<Self> {
        let file = File::new()?;
        Ok(Self { head: 0, tail: 0, file })
    }

    pub fn with_capacity(capacity: usize) -> Result<Self> {
        let file = File::with_capacity(capacity)?;
        Ok(Self { head: 0, tail: 0, file })
    }

    pub(crate) fn try_push(&mut self, layout: Layout) -> Option<NonNull<u8>> {
        let tail = self.mask(self.tail);
        let ptr = self.file.base.get(tail);

        let offset = ptr.align_offset(layout.align());
        assert!(offset < layout.size());

        let amount = offset + layout.size();

        if amount <= self.remaining() {
            self.tail += amount;
            NonNull::new(unsafe { ptr.add(offset) })
        } else {
            None
        }
    }

    pub(crate) fn try_pop(&mut self, layout: Layout) -> Option<NonNull<u8>> {
        let head = self.mask(self.head);
        let ptr = self.file.base.get(head);

        let offset = ptr.align_offset(layout.align());
        assert!(offset < layout.size());

        let amount = offset + layout.size();

        if amount <= self.len() {
            self.head += amount;
            NonNull::new(unsafe { ptr.add(offset) })
        } else {
            None
        }
    }

    pub fn push(&mut self, amount: usize) {
        debug_assert!(amount <= self.remaining());
        self.tail += amount;
    }

    pub fn pop(&mut self, amount: usize) {
        debug_assert!(amount <= self.len());
        self.head += amount;
    }

    pub fn writable(&mut self) -> &mut [u8] {
        let tail = self.mask(self.tail);
        let ptr = self.file.base.get(tail);
        let amount = self.remaining();
        unsafe { slice::from_raw_parts_mut(ptr, amount) }
    }

    pub fn readable(&mut self) -> &mut [u8] {
        let head = self.mask(self.head);
        let ptr = self.file.base.get(head);
        let amount = self.len();
        unsafe { slice::from_raw_parts_mut(ptr, amount) }
    }

    fn mask(&self, index: usize) -> usize {
        index & (self.file.capacity() - 1)
    }

    pub fn capacity(&self) -> usize {
        self.file.capacity()
    }

    pub fn len(&self) -> usize {
        debug_assert!(self.tail >= self.head);
        self.tail - self.head
    }

    pub fn remaining(&self) -> usize {
        self.file.capacity() - self.len()
    }

    pub fn is_empty(&self) -> bool {
        self.tail == self.head
    }

    pub fn is_full(&self) -> bool {
        self.len() == self.file.capacity()
    }

    pub fn clear(&mut self) {
        self.pop(self.len())
    }
}
