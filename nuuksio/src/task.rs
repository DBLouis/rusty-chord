mod id;
mod waker;

pub use {id::TaskId, waker::*};

use crate::runtime::Runtime;
use ros::{
    future::Future,
    marker::PhantomData,
    mem::ManuallyDrop,
    pin::Pin,
    prelude::*,
    ptr::NonNull,
    task::{ready, Context, Poll, Waker},
};

#[repr(C)]
pub(crate) struct Task<F: Future> {
    header: TaskHeader,
    inner: TaskFuture<F>,
}

impl<F> Task<F>
where
    F: Future + 'static,
    F::Output: 'static,
{
    pub fn new(_rt: Runtime, future: F) -> Box<Self> {
        let header = TaskHeader::new(Self::poll, Self::join, Self::drop);
        let inner = TaskFuture { future: ManuallyDrop::new(future) };
        Box::new(Self { header, inner })
    }

    pub fn into_handles(self: Box<Self>) -> (Handle<F::Output>, RawHandle) {
        let task = self.erase();
        unsafe { (Handle::new(task), RawHandle::new(task)) }
    }

    fn erase(self: Box<Self>) -> NonNull<RawTask> {
        let ptr = Box::into_raw(self) as *mut RawTask;
        // SAFETY: The pointer returned from `Box::into_raw` is never null
        unsafe { NonNull::new_unchecked(ptr) }
    }

    // SAFETY: No shared nor exclusive reference to the memory at `task` must be held.
    unsafe fn poll(task: NonNull<RawTask>, cx: &mut Context<'_>) -> Poll<()> {
        let mut task = task.cast::<Self>();

        // This is only verified in development builds because
        // a failure here would indicate a bug in the runtime!
        debug_assert!(!task.as_ref().header.is_borrowed());

        // Task is not borrowed, we can take a mutable reference of it.
        let task = task.as_mut();

        let future = Pin::new_unchecked(&mut *task.inner.future);

        let output = ready!({
            task.header.toggle_borrowed();
            let result = future.poll(cx);
            task.header.toggle_borrowed();
            result
        });

        // The future is ready, we drop it in place...
        ManuallyDrop::drop(&mut task.inner.future);
        // .. and overwrite it with its output
        task.inner.output = ManuallyDrop::new(output);

        if let Some(waker) = task.header.waker.take() {
            waker.wake();
        }

        task.header.set_ready();
        Poll::Ready(())
    }

    // SAFETY: No shared nor exclusive reference to the memory at `task` must be held.
    unsafe fn join(task: NonNull<RawTask>, cx: &mut Context<'_>) -> Poll<NonNull<()>> {
        let mut task = task.cast::<Self>();
        debug_assert!(!task.as_ref().header.is_borrowed());

        // Task is not borrowed, we can take a mutable reference of it.
        let task = task.as_mut();

        if task.header.is_ready() {
            let output = NonNull::new_unchecked(&mut *task.inner.output);
            Poll::Ready(output.cast())
        } else {
            // We store out waker in the task header and we'll awake when task is ready
            let waker = cx.waker().clone();
            let _ = task.header.waker.replace(waker);
            Poll::Pending
        }
    }

    // SAFETY: No shared nor exclusive reference to the memory at `task` must be held.
    unsafe fn drop(task: NonNull<RawTask>) {
        let mut task = task.cast::<Self>();
        debug_assert!(!task.as_ref().header.is_borrowed());

        // Task is not borrowed, we can take a mutable reference of it.
        let task = task.as_mut();

        if task.header.is_detached() {
            debug_assert!(task.header.is_ready());
            let _ = Box::from_raw(task);
        } else {
            task.header.detach();
        }
    }
}

// Detached bit
const D_BIT: u8 = 0x1;
// Ready bit
const R_BIT: u8 = 0x2;
// Mutable borrow bit
const B_BIT: u8 = 0x4;

type TaskPollFn = unsafe fn(task: NonNull<RawTask>, cx: &mut Context<'_>) -> Poll<()>;
type TaskJoinFn = unsafe fn(task: NonNull<RawTask>, cx: &mut Context<'_>) -> Poll<NonNull<()>>;
type TaskDropFn = unsafe fn(task: NonNull<RawTask>);

struct TaskHeader {
    status: u8,
    waker: Option<Waker>,
    poll_fn: TaskPollFn,
    join_fn: TaskJoinFn,
    drop_fn: TaskDropFn,
}

impl TaskHeader {
    fn new(poll_fn: TaskPollFn, join_fn: TaskJoinFn, drop_fn: TaskDropFn) -> Self {
        Self { status: 0, waker: None, poll_fn, join_fn, drop_fn }
    }

    fn is_detached(&self) -> bool {
        (self.status & D_BIT) != 0
    }

    fn detach(&mut self) {
        self.status |= D_BIT;
    }

    fn is_ready(&self) -> bool {
        (self.status & R_BIT) != 0
    }

    fn set_ready(&mut self) {
        self.status |= R_BIT;
    }

    fn is_borrowed(&self) -> bool {
        (self.status & B_BIT) != 0
    }

    fn toggle_borrowed(&mut self) {
        self.status ^= B_BIT;
    }
}

#[repr(C)]
union TaskFuture<F: Future> {
    future: ManuallyDrop<F>,
    output: ManuallyDrop<F::Output>,
}

#[repr(C)]
struct RawTask {
    header: TaskHeader,
}

impl RawTask {
    #[inline]
    unsafe fn poll(task: NonNull<Self>, cx: &mut Context<'_>) -> Poll<()> {
        let poll_fn = task.cast::<TaskHeader>().as_ref().poll_fn;
        (poll_fn)(task, cx)
    }

    #[inline]
    unsafe fn join(task: NonNull<Self>, cx: &mut Context<'_>) -> Poll<NonNull<()>> {
        let join_fn = task.cast::<TaskHeader>().as_ref().join_fn;
        (join_fn)(task, cx)
    }

    #[inline]
    unsafe fn drop(task: NonNull<Self>) {
        let drop_fn = task.cast::<TaskHeader>().as_ref().drop_fn;
        (drop_fn)(task);
    }
}

/// Typed handle to a Task.
// This is the public type allowing to join or cancel a task.
#[derive(Debug)]
pub struct Handle<R> {
    task: NonNull<RawTask>,
    output: PhantomData<R>,
}

impl<R> Handle<R> {
    unsafe fn new(task: NonNull<RawTask>) -> Self {
        Self { task, output: PhantomData }
    }

    pub fn tid(&self) -> TaskId {
        // The task identifier is its heap address.
        TaskId::from(self.task.as_ptr() as usize)
    }

    pub fn cancel(self) {
        todo!()
    }
}

impl<R> Drop for Handle<R> {
    fn drop(&mut self) {
        unsafe { RawTask::drop(self.task) }
    }
}

impl<R> Future for Handle<R> {
    type Output = R;

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        unsafe { RawTask::join(self.task, cx).map(|o| o.cast::<R>().as_ptr().read()) }
    }
}

/// Type-erased handle to a Task.
// This is the internal type for the executor
#[derive(Debug)]
pub struct RawHandle {
    task: NonNull<RawTask>,
}

impl RawHandle {
    unsafe fn new(task: NonNull<RawTask>) -> Self {
        Self { task }
    }

    pub fn tid(&self) -> TaskId {
        // The task identifier is its heap address.
        TaskId::from(self.task.as_ptr() as usize)
    }

    // called by executor
    pub fn poll(&self, cx: &mut Context<'_>) -> Poll<()> {
        unsafe { RawTask::poll(self.task, cx) }
    }
}

impl Drop for RawHandle {
    fn drop(&mut self) {
        unsafe { RawTask::drop(self.task) }
    }
}
