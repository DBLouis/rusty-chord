use crate::{reactor::Interest, runtime::Runtime};
use ros::{
    error::{Error, Result},
    fmt,
    future::poll_fn,
    io::Fd,
    mem,
    mem::MaybeUninit,
    prelude::*,
    ptr,
    ptr::addr_of,
    task::{Context, Poll},
};

#[repr(transparent)]
pub struct SignalSet(libc::sigset_t);

impl SignalSet {
    pub fn empty() -> Self {
        let mut set = MaybeUninit::uninit();
        let rv = unsafe { libc::sigemptyset(set.as_mut_ptr()) };
        if rv == -1 {
            panic!("{}", Error::last());
        } else {
            let set = unsafe { set.assume_init() };
            Self(set)
        }
    }

    pub fn add(&mut self, signal: i32) {
        let rv = unsafe {
            let set = &mut self.0 as *mut _;
            libc::sigaddset(set, signal)
        };
        if rv == -1 {
            panic!("{}", Error::last());
        }
    }

    pub fn contains(&self, signal: i32) -> bool {
        let rv = unsafe {
            let set = &self.0 as *const _;
            libc::sigismember(set, signal)
        };

        if rv == -1 {
            panic!("{}", Error::last());
        } else {
            rv != 0
        }
    }
}

impl fmt::Debug for SignalSet {
    fn fmt(&self, _formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        todo!()
    }
}

pub struct SignalHandler {
    rt: Runtime,
    mask: SignalSet,
    fd: Fd,
    callback: Box<dyn FnMut(i32)>,
}

impl SignalHandler {
    pub fn new(mask: SignalSet, callback: Box<dyn FnMut(i32)>) -> Result<Self> {
        let rt = Runtime::get();

        let rv = unsafe {
            let mask = &mask.0 as *const _;
            libc::pthread_sigmask(libc::SIG_SETMASK, mask, ptr::null_mut())
        };

        if rv != 0 {
            return Err(Error::from(rv));
        }

        let flags = libc::SFD_NONBLOCK | libc::SFD_CLOEXEC;
        let fd = unsafe {
            let mask = &mask.0 as *const _;
            libc::signalfd(-1, mask, flags)
        };

        if fd == -1 {
            Err(Error::last())
        } else {
            Ok(Self { rt, mask, fd, callback })
        }
    }

    pub async fn catch(mut self) {
        loop {
            let signal = self.read().await;

            if self.mask.contains(signal) {
                (self.callback)(signal)
            }
        }
    }

    async fn read(&mut self) -> i32 {
        poll_fn(move |cx| poll_read(self.rt, cx, self.fd)).await
    }
}

impl fmt::Debug for SignalHandler {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        formatter
            .debug_struct("SignalHandler")
            .field("mask", &self.mask)
            .field("fd", &self.mask)
            .field("callback", &addr_of!(self.callback))
            .finish()
    }
}

fn poll_read(rt: Runtime, cx: &mut Context<'_>, fd: Fd) -> Poll<i32> {
    let mut out = unsafe { mem::zeroed::<libc::signalfd_siginfo>() };

    let rv = unsafe {
        let out = &mut out as *mut _ as *mut _;
        libc::read(fd, out, mem::size_of::<libc::signalfd_siginfo>())
    };

    let result = if rv == -1 { Err(Error::last()) } else { Ok(out.ssi_signo as i32) };

    match result {
        Err(Error::Again) => {
            rt.register(fd, Interest::readable(), cx.waker().clone());
            Poll::Pending
        }
        Err(e) => panic!("{:?}", e),
        Ok(signal) => Poll::Ready(signal),
    }
}
