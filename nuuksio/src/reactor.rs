use crate::prelude::*;
use ros::{
    collections::HashMap,
    error::{Error, Result},
    fmt,
    io::Fd,
    mem,
    prelude::*,
    ptr,
    task::Waker,
};

const BUFFER_SIZE: usize = 128;

#[derive(Debug)]
pub struct Reactor {
    fd: Fd,
    registry: HashMap<Fd, Waker>,
    buffer: Vec<Event>,
}

impl Reactor {
    pub fn new() -> Result<Self> {
        let fd = unsafe { libc::epoll_create1(libc::EPOLL_CLOEXEC) };
        if fd == -1 {
            return Err(Error::last());
        }

        let registry = HashMap::new();
        let buffer = vec![Event::empty(); BUFFER_SIZE];

        Ok(Self { fd, registry, buffer })
    }

    pub fn register(&mut self, fd: Fd, interest: Interest, waker: Waker) {
        debug_assert!(fd > 0);

        let mut event = Event::new(interest.flags | libc::EPOLLET | libc::EPOLLONESHOT, fd as u64);

        let op = if self.registry.insert(fd, waker).is_none() { libc::EPOLL_CTL_ADD } else { libc::EPOLL_CTL_MOD };

        let rv = unsafe { libc::epoll_ctl(self.fd, op, fd, &mut event as *mut _ as *mut _) };
        assert_ne!(rv, -1);
    }

    pub fn unregister(&mut self, fd: Fd) {
        // Ignore errors
        let _ = self.registry.remove(&fd);
        let _ = unsafe { libc::epoll_ctl(self.fd, libc::EPOLL_CTL_DEL, fd, ptr::null_mut()) };
    }

    pub fn wake_ready(&mut self) -> Result<()> {
        let count = epoll_wait(self.fd, &mut self.buffer)?;
        if count == 0 {
            return Err(Error::TimedOut);
        }

        let iter = self.buffer.iter().take(count).map(|e| e.into_fd());

        for fd in iter {
            if let Some(waker) = self.registry.get(&fd) {
                waker.wake_by_ref()
            } else {
                warn!("no waker for fd {}", fd);
            }
        }

        Ok(())
    }
}

impl Drop for Reactor {
    fn drop(&mut self) {
        let _ = unsafe { libc::close(self.fd) };
    }
}

impl fmt::Display for Reactor {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(formatter, "Reactor({})", self.fd)
    }
}

#[derive(Clone, Copy)]
#[repr(transparent)]
struct Event {
    inner: libc::epoll_event,
}

impl Event {
    fn new(flags: i32, data: u64) -> Self {
        Self { inner: libc::epoll_event { events: flags as u32, u64: data } }
    }

    fn empty() -> Self {
        unsafe { mem::zeroed() }
    }

    fn into_fd(self) -> Fd {
        self.inner.u64 as Fd
    }
}

impl fmt::Debug for Event {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        formatter.debug_struct("Event").finish_non_exhaustive()
    }
}

#[derive(Debug)]
#[repr(transparent)]
pub struct Interest {
    flags: i32,
}

impl Interest {
    pub const fn readable() -> Self {
        Self { flags: libc::EPOLLIN }
    }
    pub const fn writable() -> Self {
        Self { flags: libc::EPOLLOUT }
    }
}

fn epoll_wait(fd: Fd, buffer: &mut [Event]) -> Result<usize> {
    // Safety: Event is repr(transparent) of epoll_event
    let rv = unsafe {
        let buffer = buffer.as_mut_ptr() as *mut _;
        libc::epoll_wait(fd, buffer, BUFFER_SIZE as i32, 11111)
    };

    if rv == -1 {
        Err(Error::last())
    } else {
        debug_assert!(rv >= 0);
        Ok(rv as usize)
    }
}

impl fmt::Display for Interest {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self.flags {
            libc::EPOLLIN => write!(formatter, "readable"),
            libc::EPOLLOUT => write!(formatter, "writable"),
            _ => unreachable!(),
        }
    }
}
