use either::Either;
use pin_project_lite::pin_project;
use ros::{
    future::Future,
    pin,
    pin::Pin,
    task::{Context, Poll},
};

pub async fn yield_now() {
    struct YieldNow {
        yielded: bool,
    }

    impl Future for YieldNow {
        type Output = ();

        fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<()> {
            if self.yielded {
                return Poll::Ready(());
            }

            self.yielded = true;
            cx.waker().wake_by_ref();
            Poll::Pending
        }
    }

    YieldNow { yielded: false }.await
}

// biased toward `left`
pub async fn race<L, R>(left: L, right: R) -> Either<L::Output, R::Output>
where
    L: Future,
    R: Future,
{
    pin_project! {
        struct Race<L, R> {
            #[pin]
            left: L,
            #[pin]
            right: R,
        }
    }

    impl<L, R> Future for Race<L, R>
    where
        L: Future,
        R: Future,
    {
        type Output = Either<L::Output, R::Output>;

        fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
            let this = self.project();
            match this.left.poll(cx) {
                Poll::Pending => this.right.poll(cx).map(Either::Right),
                ready => ready.map(Either::Left),
            }
        }
    }

    Race { left, right }.await
}

pub async fn zip<A, B>(a: A, b: B) -> (A::Output, B::Output)
where
    A: Future,
    B: Future,
{
    pin!(a);
    pin!(b);
    match race(&mut a, &mut b).await {
        Either::Left(a) => (a, b.await),
        Either::Right(b) => (a.await, b),
    }
}

pub async fn try_zip<A, B, E, T, U>(a: A, b: B) -> Result<(T, U), E>
where
    A: Future<Output = Result<T, E>>,
    B: Future<Output = Result<U, E>>,
{
    pin!(a);
    pin!(b);
    match race(&mut a, &mut b).await {
        Either::Left(a) => b.await.and_then(|b| a.map(|a| (a, b))),
        Either::Right(b) => a.await.and_then(|a| b.map(|b| (a, b))),
    }
}
