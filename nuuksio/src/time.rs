use crate::{log::Level, prelude::*, reactor::Interest, runtime::Runtime};
use pin_project_lite::pin_project;
use ros::{
    error::{Error, Result},
    future::Future,
    io::Fd,
    pin::Pin,
    ptr,
    task::{Context, Poll},
    time::{Duration, Expiration, Instant},
};

mod_level!(Level::Debug);

pub async fn sleep(duration: Duration) -> Result<()> {
    let timer = Timer::one_shot(duration)?;
    timer.await
}

pub async fn sleep_until(deadline: Instant) -> Result<()> {
    let timer = Timer::at(deadline)?;
    timer.await
}

pub fn interval(period: Duration) -> Result<Interval> {
    let timer = Timer::interval(period)?;
    Ok(Interval { timer })
}

pub fn interval_at(start: Instant, period: Duration) -> Result<Interval> {
    let timer = Timer::interval_at(start, period)?;
    Ok(Interval { timer })
}

#[derive(Debug)]
pub struct Interval {
    timer: Timer,
}

impl Interval {
    pub async fn tick(&mut self) {
        let timer = Pin::new(&mut self.timer);
        let _ = timer.await;
    }
}

pub async fn timeout<F>(duration: Duration, future: F) -> Result<F::Output>
where
    F: Future,
{
    let timer = Timer::one_shot(duration)?;
    let timeout = Timeout { timer, future };
    timeout.await
}

pin_project! {
    struct Timeout<F> {
        timer: Timer,
        #[pin]
        future: F,
    }
}

impl<F> Future for Timeout<F>
where
    F: Future,
{
    type Output = Result<F::Output>;

    fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let mut this = self.as_mut().project();

        match Pin::new(&mut this.timer).poll(cx) {
            Poll::Ready(_) => Poll::Ready(Err(Error::TimedOut)),
            Poll::Pending => this.future.poll(cx).map(Ok),
        }
    }
}

#[derive(Debug)]
#[repr(transparent)]
struct Timer {
    rt: Runtime,
    fd: Fd,
}

impl Timer {
    fn interval(duration: Duration) -> Result<Self> {
        Self::new(Expiration::interval(duration), false)
    }

    fn interval_at(instant: Instant, duration: Duration) -> Result<Self> {
        Self::new(Expiration::interval_at(instant, duration), true)
    }

    fn one_shot(duration: Duration) -> Result<Self> {
        Self::new(Expiration::one_shot(duration), false)
    }

    fn at(instant: Instant) -> Result<Self> {
        Self::new(Expiration::at(instant), true)
    }

    fn new(expiration: Expiration, absolute: bool) -> Result<Self> {
        let rt = Runtime::get();
        mod_trace!(rt; "new timer with {:?}", expiration);

        let flags = libc::TFD_NONBLOCK | libc::TFD_CLOEXEC;

        let fd = unsafe { libc::timerfd_create(libc::CLOCK_MONOTONIC, flags) };
        if fd == -1 {
            return Err(Error::last());
        }

        let flags = if absolute { libc::TFD_TIMER_ABSTIME } else { 0 };

        let rv = unsafe {
            let arg = &*expiration as *const _;
            libc::timerfd_settime(fd, flags, arg, ptr::null_mut())
        };
        if rv == -1 {
            return Err(Error::last());
        }

        Ok(Self { rt, fd })
    }

    fn wait(&mut self) -> Result<()> {
        mod_trace!(self.rt; "timer wait");
        let mut count = 0u64;
        if unsafe { libc::read(self.fd, &mut count as *mut _ as *mut libc::c_void, 8) } == -1 {
            Err(Error::last())
        } else {
            debug_assert_ne!(count, 0);
            Ok(())
        }
    }
}

impl Future for Timer {
    type Output = Result<()>;

    fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        match self.wait() {
            Err(Error::WouldBlock) => {
                self.rt.register(self.fd, Interest::readable(), cx.waker().clone());
                Poll::Pending
            }
            Err(e) => panic!("{}", e),
            ok => Poll::Ready(ok),
        }
    }
}

impl Drop for Timer {
    fn drop(&mut self) {
        self.rt.unregister(self.fd);
    }
}
